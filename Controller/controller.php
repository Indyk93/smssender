<?php
class Controller{

	private $content;
	
	function __construct()
	{
		$this->content = file_get_contents("View/main.html");
		if(isset($_SESSION['login']))$this->content = str_replace("::login::", '<a href="?page=login&action=logout"><li>Wyloguj</li></a>', $this->content);
		else $this->content = str_replace("::login::", '<a href="?page=login&action=login"><li>Loguj</li></a>', $this->content);
	}
	
	function show_content($str)
	{
		$this->content = str_replace('::content::', $str, $this->content);
		echo $this->content;
	}
	
	function error($param)
	{
		$str = '<font color="red"><b>'.$param.'</b></font>';
		$this->show_content($str);
	}
	
	
}


?>