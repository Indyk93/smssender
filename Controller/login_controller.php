<?php
require_once('Model/userservice.php');
require_once("controller.php");

class LoginController extends Controller{
	
	function login()
	{
		$login = $_POST['login'];
		$password = $_POST['password'];
		$ses_login = $_SESSION['login'];
		if(!isset($login) || !isset($password) &&!isset($ses_login))
		{
			$str = file_get_contents("View/login_form.html");
		}else{
			if(!isset($ses_login))
			{
				$service = new UserService();
				$str = $service->loginValidation($login, $password);
			}else header("Location: index.php");
		}
		$this->show_content($str);
	}
	
	function logout()
	{
		session_destroy();
		$str = '<center><b>Wylogowano</b></center>';
		$this->show_content($str);
	}
	
}


?>