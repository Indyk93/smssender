<?php
require_once('Model/userservice.php');
require_once("controller.php");

class UsersController extends Controller{
	
	private $service;
	
	function __construct()
	{
		parent::__construct();
		$this->service = new UserService();
	}
	
	function management()
	{
		if(!isset($_SESSION['login']))header("Location: index.php");
		$str = "<table><tr><td>Login</td><td>Access</td><td>Edit</td><td>Delete</td></tr>";
		
		$str = $str.$this->service->getUsersList();
		$str = $str."</table>";
		$this->show_content($str);
	}
	
	function edit($id)
	{
		$pass1 = $_POST['pass1'];
		$pass2 = $_POST['pass2'];
		
		if(isset($pass1) && isset($pass2))
		{
			if(strcmp($pass1, $pass2)==0)
			{
				$str = $this->service->editUser($id, $pass);
			}else{
			$str = "Hasla nie sa identyczne";
			}
		}else{
			$form = file_get_contents("View/user_edit_form.html");
			$form = str_replace("::id::", $id, $form);
			$str = $form;
		}
		$this->show_content($str);
	}
	
	function add()
	{	
		$pass1 = $_POST['pass1'];
		$pass2 = $_POST['pass2'];
		$login = $_POST['login'];
		
		if(isset($pass1)&&isset($pass2)&&isset($login)&&strcmp($pass1, $pass2) == 0)
		{
			$str = $this->service->addUser($login, $pass);
		}else{
			$str = file_get_contents("View/user_add_form.html");
		}
		$this->show_content($str);	
	}
	
	function deleteu($id)
	{
		$str = $this->service->deleteUser($id);
		$this->show_content($str);	
	}
	
}
?>