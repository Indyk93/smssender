<?php
require_once('Model/contactsservice.php');
require_once("controller.php");

class ContactsController extends Controller{
	
	private $service;
	
	function __construct()
	{
		parent::__construct();
		$this->service = new ContactsService();
	}
	
	function management()
	{
		if(!isset($_SESSION['login']))header("Location: index.php");
		$str = "<table><tr><td>Id</td><td>Numer</td><td>Wlasciciel</td><td>Edit</td><td>Delete</td></tr>";
		
		$str = $str.$this->service->getContactsList();
		
		$str = $str."</table>";
		$this->show_content($str);
	}
	
	function edit($id)
	{
		$owner = $_POST['owner'];
		$number = $_POST['number'];
		
		if(isset($number) && isset($owner))
		{
			$str = $this->service->editContact($id, $owner, $number);
		}else{
			$form = file_get_contents("View/contact_edit_form.html");
			$str = $this->service->prepareEditForm($id, $form);
		}
		$this->show_content($str);
	}
	
	function add()
	{	
		$owner = $_POST['owner'];
		$number = $_POST['number'];
		
		if(isset($number)&&isset($owner))
		{
			$str = $this->service->addContact($owner, $number);
		}else{
			$str = file_get_contents("View/contact_add_form.html");
		}
		$this->show_content($str);	
	}
	
	function deletec($id)
	{
		$str = $this->service->deleteContact($id);
		$this->show_content($str);	
	}
	
}
?>