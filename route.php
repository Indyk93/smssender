<?php

	route($page, $action, $action_param);

	function route($page, $action, $action_param)
	{
		require_once("Controller/".$page."_controller.php");
		switch($page)
		{
			case "home":
				$controller = new HomeController();
				break;
			case "login":
				$controller = new LoginController();
				break;
			case "users":
				$controller = new UsersController();
				break;
			case "contacts":
				$controller = new ContactsController();
				break;
			case "sms":
				$controller = new SmsController();
				break;
		}
		if($action!="")$controller->$action($action_param);else
			$controller->hello();
	}


?>
