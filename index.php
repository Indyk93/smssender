<?php
session_start();

if(!isset($_GET['page']) || $_GET['page'] == "home"){
	$page = "home";
	if(isset($_GET['action'])){
		$action = $_GET['action'];
	}else{ $action = "";}
}else{
	$page = $_GET['page'];
	$action = $_GET['action'];
}
if(isset($_GET['action_param']))$action_param = $_GET['action_param']; else $action_param = "";

$present_controllers = array("home" => ["error", "hello"], 
							"login" => ["error", "login", "logout"],
							"users" => ["error", "management", "add", "deleteu", "edit"],
							"contacts" => ["error", "management", "add", "deletec", "edit"],
							"sms" => ["error", "view_contacts", "send"]);

if(!array_key_exists($page, $present_controllers)){
	$page = "home";
	$action = "error";
	$action_param = "Bledna podstrona";
}else{
	if(!in_array($action, $present_controllers[$page]) && !($action==""))
	{
		$page = "home";
		$action = "error";
		$action_param = "Bledna operacja";
	}
}

require_once('route.php');

?>