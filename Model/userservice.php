<?php
require_once('Model/userrepository.php');
require_once('Model/userdao.php');

class UserService
{
	private $repo;
	
	function __construct()
	{
		$this->repo = new UsersRepository();
	}
	
	function loginValidation($login, $password)
	{
		$login = addslashes($login);
		$password = addslashes($password);
		$user = $this->repo->validateUser($login, $password);
		if($user)
		{
			if($user->getAccess() > 0)
			{
				$_SESSION['login'] = $user->getLogin();
				$user->setSessionId(session_id());
				return "Zalogowano jako: ".$_SESSION['login'];
			}else{
				return "Bledne dane logowania";
			}
		}
	}
	
	function getUsersList()
	{
		$result = $this->repo->findAll();
		$str = "";
		foreach($result as $user)
		{
			$row = "<tr><td>".$user->getLogin()."</td><td>".$user->getAccess().'</td><td><a href="?page=users&action=edit&action_param='.$user->getId().'">Edit</a></td><td><a href="?page=users&action=deleteu&action_param='.$user->getId().'">Delete</a></td></tr>';
			$str = $str.$row;
		}
		return $str;
	}
	
	function editUser($id, $pass)
	{
		$pass = addslashes($pass);
		$admin = $this->repo->findUserByLogin($_SESSION['login']);
		if($admin->getSessionId() == session_id() && $admin->getAccess() > 1)
		{
			$edited_user = $this->repo->findUserById($id);
			if($edited_user->setPassword($pass)){$str = "Dane zmienione"; } else {$str = "Wystapil blad w trakcie zmiany danych";}
		}else{
			$str = "Nie posiadasz uprawnien";
		}	
		return $str;
	}
	
	function addUser($login, $pass)
	{
		$login = addslashes($login);
		$admin = $repo->findUserByLogin($_SESSION['login']);
		if($admin->getSessionId() == session_id() && $admin->getAccess() > 1)
		{
			$new_user = new UserDAO();
			$new_user->makeNew($login, $pass1);
			if($repo->addUser($new_user))$str = "Uzytkownik dodany"; else $str = "Blad w trakcie dodawania nowego uzytkownika";
		}else{
			$str = "Brak uprawnien";
		}
		return $str;
	}
	
	function deleteUser($id)
	{
		$admin = $repo->findUserByLogin($_SESSION['login']);
		if($admin->getSessionId() == session_id() && $admin->getAccess() > 1)
		{
			if($repo->deleteUserById($id)){$str = "Usuwanie zakoczone powodzeniem";} else{$str = "Usuwanie sie nie powiodlo";}
		}else{
			$str = "Brak uprawnien";
		}
		return $str;
	}
}


?>