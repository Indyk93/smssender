<?php
require_once('Model/contactsrepository.php');
require_once('Model/contactdao.php');
require_once('Model/userrepository.php');

class ContactsService
{
	private $repo;
	private $users;
	
	function __construct()
	{
		$this->repo = new ContactsRepository();
		$this->users = new UsersRepository();
	}
	
	function getContactsList()
	{
		$result = $this->repo->findAll();
		$str = "";
		foreach($result as $contact)
		{
			$row = "<tr><td>".$contact->getId()."</td><td>".$contact->getPNumber().'</td><td>'.$contact->getOwner().'</td><td><a href="?page=contacts&action=edit&action_param='.$contact->getId().'">Edit</a></td><td><a href="?page=contacts&action=deletec&action_param='.$contact->getId().'">Delete</a></td></tr>';
			$str = $str.$row;
		}
		return $str;
	}
	
	function prepareEditForm($id, $form)
	{
		$contact = $this->repo->findContactById($id);
		$form = str_replace("::id::", $id, $form);
		$form = str_replace("::number::", $contact->getPNumber(), $form);
		$form = str_replace("::owner::", $contact->getOwner(), $form);
		return $form;

	}
	
	function editContact($id, $owner, $number)
	{
		$admin = $this->users->findUserByLogin($_SESSION['login']);
		if($admin->getSessionId() == session_id() && $admin->getAccess() > 1)
		{
			$contact = $this->repo->findContactById($id);
			$status1 = $contact->setOwner($owner);
			$status2 = $contact->setPNumber($number);
			if($status1 && $status2){$str = "Dane zmienione"; } else {$str = "Wystapil blad w trakcie zmiany danych";}
		}else{
			$str = "Nie posiadasz uprawnien";
		}	
		return $str;
	}
	
	function addContact($owner, $number)
	{
		$admin = $this->users->findUserByLogin($_SESSION['login']);
		if($admin->getSessionId() == session_id() && $admin->getAccess() > 1)
		{
			$new_contact = new ContactDAO();
			$new_contact->makeNew($owner, $number);
			if($this->repo->addContact($new_contact))$str = "Kontakt dodany"; else $str = "Blad w trakcie dodawania nowego uzytkownika";
		}else{
			$str = "Brak uprawnien";
		}
		return $str;
	}
	
	function deleteContact($id)
	{
		$admin = $this->users->findUserByLogin($_SESSION['login']);
		if($admin->getSessionId() == session_id() && $admin->getAccess() > 1)
		{
			if($this->repo->deleteContactById($id)){$str = "Usuwanie zakoczone powodzeniem";} else{$str = "Usuwanie sie nie powiodlo";}
		}else{
			$str = "Brak uprawnien";
		}
		return $str;
	}
}


?>