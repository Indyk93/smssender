<?php
require_once('Model/smsrepository.php');
require_once('Model/contactsrepository.php');
require_once('Model/userrepository.php');

class SmsService
{
	private $repo;
	private $users;
	private $contact;
	
	function __construct()
	{
		$this->repo = new SmsRepository();
		$this->users = new UsersRepository();
		$this->contact = new ContactsRepository();
	}
	
	function getContactsList()
	{
		$result = $this->contact->findAll();
		$str = "";

		foreach($result as $contact)
		{
			$row = "<tr><td>".$contact->getPNumber().'</td><td>'.$contact->getOwner().'</td><td><input type="checkbox" name="checkbox'.$contact->getId().'" value="sendto'.$contact->getId().'"></td></tr>';
			$str = $str.$row;
		}
		return $str;
	}
	
	function getSmsTargetList($post)
	{
		$iterator = 0;
		$result_list = [];
		foreach($post as $record)
		{
			if(strpos($record, "sendto")!==false)
			{
				$result_list[$iterator] = str_replace("sendto", "", $record);
				$iterator +=1;
			}
		}
		if(count($result_list)>0)return $result_list; else return null;
	}
	
	function sendSMS($list, $time, $text)
	{
		$date_time = new DateTime($date);
		$date_time = $date_time->getTimestamp();
		$present_time = time();
		if($date_time-$present_time >= 0)
		{
			$str = "SMSy zapisane do wysłania";
			foreach($list as $user)
			{
				$sms_sent = $this->repo->findAwaitingSmsByContact($user);
				if($sms_sent == 0){
					$sms = new SmsDAO();
					$sms->makeNew($user, $text, $time);
					$this->repo->addSMS($sms);
					$str = $str."<br> Wysłane do id:".$user;
				}else{
					$str = $str."<br> Nie wysłane do id:".$user.", inny sms oczekuje na wysłanie do tej osoby.";
				}
			}
		}else{
			$str = "Czas wyslania nie moze poprzedzac obecnego";
		}
		return $str;
	}
}


?>