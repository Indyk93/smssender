<?php
class UserDAO
{
	private $id;
	private $login;
	private $password;
	private $access;
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getLogin()
	{
		return $this->login;
	}
	
	public function getPassword()
	{
		return $this->password;
	}
	
	public function getAccess()
	{
		return $this->access;
	}
}


?>