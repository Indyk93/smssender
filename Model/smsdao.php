<?php
class SmsDAO
{
	private $id;
	private $contact_id;
	private $text;
	private $time;
	private $send;
		
	public function getId()
	{
		return $this->id;
	}
	
	public function getContactId()
	{
		return $this->contact_id;
	}
	
	public function getText()
	{
		return $this->text;
	}
	
	public function getTime()
	{
		return $this->time;
	}
	
	public function isSend()
	{
		return $this->send;
	}

	public function makeNew($target, $text, $time)
	{
		$this->contact_id = $target;
		$this->text = $text;
		$this->time = $time;
	}
}


?>